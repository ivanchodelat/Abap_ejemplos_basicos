*&---------------------------------------------------------------------*
*& Report ycontrol_scp
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ycontrol_scp.

DATA:    gs_encabezado TYPE yty_encabezado.  "Estructura del encabezado de pÃ¡gina
DATA     gs_pie        TYPE yty_pie.         "Estructura Pie de pÃ¡gina.
DATA     gt_materiales TYPE STANDARD TABLE OF yty_materiales.
*..
DATA: modulo_funcion TYPE rs38l_fnam.
**********************************************************************
INCLUDE  ycontrol_scp_lcl.
**********************************************************************
TABLES:
  mard.       "Datos de almacén para el material
*PARAMETERS:
*  p_werks TYPE werks_d,      "Centro
*  p_lgort TYPE lgort_d.      "AlmacÃ©n MM
*SELECT-OPTIONS:
*    s_matnr  FOR mard-matnr.   "Material

START-OF-SELECTION.
*.. Datos de la tabla

  TYPES  ty_matnr_range      TYPE  RANGE OF matnr.      "Número de la cuenta de mayor
  DATA(lv_matnr_range) = VALUE ty_matnr_range(  ( sign = 'I' option = 'CP' low = '00000000000008*' ) ).

  SELECT a~matnr,b~maktx  INTO TABLE @gt_materiales
        FROM mard AS a INNER JOIN makt AS b
                       ON  a~matnr EQ b~matnr
        WHERE werks = '1000'
        AND   lgort = '1000'
        AND   spras = @sy-langu
        AND   a~matnr IN @lv_matnr_range.
*..Datos de cabecera
  gs_encabezado-werks = 'Centro: 1000'.
  gs_encabezado-lgort = 'Almacén: 1000'.

*..datos de pie
  gs_pie-datum = sy-datlo.
  gs_pie-uname = sy-uname.

  "  cl_demo_output=>display( gt_materiales ).
  TRY.
*.. Parámetros de impresión
      lcl_imprime=>set_file_name( 'Ivan_file' ).
      lcl_imprime=>set_nuevo_spool( ).
*-----PAG 1---------------------------------
      gs_encabezado-werks = '1001'.
      lcl_imprime=>smf_standard_print( ).
      DATA(id_spool) = lcl_imprime=>get_spool_id( ).
*-----PAG 2 --------------------------------
      lcl_imprime=>set_fin_spool( )." pagina final
      gs_encabezado-werks = '1002'.
      lcl_imprime=>smf_standard_print( ).
      lcl_imprime=>spool_to_pdf( id_spool ).
*.. Problema al imprimir
    CATCH cx_abap_docu_download INTO DATA(cx_docu).
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
         WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*.. Something bad happened
    CATCH cx_root INTO DATA(gcx_root).
      MESSAGE i499(sy) WITH gcx_root->get_text( ) DISPLAY LIKE 'E'.
  ENDTRY.

END-OF-SELECTION.