****************************************************
* Metodo que llama la impresion
****************************************************
  METHOD write_file_rais_if_error.

    "!Total de lineas en la tabla it_giro, que hay que dividir en hojas
    DATA lv_total_lineas TYPE i.
    "! Numero de hojas del reporte
    DATA lv_no_total_de_hojas  TYPE i.
    "! Numero de hoja Acutual
    DATA  lv_hoja_actual TYPE i.
    "!Numero de linea
    DATA  lv_linea_actual   TYPE i.
    "!Numero del Spool
    DATA spool_id  TYPE tsp01-rqident.
    lcl_functions=>c_lineas_x_hoja = 18.
*............................................*
    TRY.
        lcl_imprimir=>set_parametros_impresion( ).
        lv_no_total_de_hojas = lcl_functions=>calcula_no_hojas( lcl_functions=>c_lineas_x_hoja ).
        lv_hoja_actual = 0.
        lcl_imprimir=>st_output_options-tdnewid = 'X'. "Nuevo Spool. MUY IMPORTANTE
        WHILE lv_hoja_actual < lv_no_total_de_hojas .
          lv_hoja_actual = lv_hoja_actual + 1.
          CLEAR gt_giro_hoja.
          lcl_functions=>llena_cada_hoja( lv_hoja_actual ).
          gs_heder1-orden_giro = lcl_functions=>get_consecutivo( ).
          gs_suma = lcl_functions=>suma_hoja( ).
          lcl_functions=>completa_18_lineas( ).
          lcl_functions=>save_to_ztfi_007_004( gs_heder1-orden_giro ).
          """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
          spool_id = lcl_imprimir=>envia_smf_al_spool( ). "Envia al Spool
          """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
          lcl_imprimir=>st_output_options-tdnewid = ' '. "Nuevo Spool Apgado. MUY IMPORTANTE

        ENDWHILE.
      CATCH cx_abap_docu_download INTO DATA(cx_docu).
        RAISE EXCEPTION NEW cx_abap_docu_download( previous = cx_docu ).
      CATCH   cx_qie_no_number_range   INTO DATA(cx_range).
        RAISE EXCEPTION NEW cx_qie_no_number_range( previous = cx_docu ).
    ENDTRY.
*         .............En el PC ......................
    TRY.
        IF gf_on_serv EQ abap_false.

          lcl_imprimir=>set_file_name( gv_file_name ).
          lcl_imprimir=>envia_spool_a_pdf( spool_id ).

        ELSE.
*        .............En el Servidor ......................

          lcl_imprimir=>set_file_name( gv_file_name ).
          lcl_imprimir=>envia_spool_a_pdf_server( spool_id ).

        ENDIF.
      CATCH cx_abap_docu_download INTO cx_docu.
        RAISE EXCEPTION NEW cx_abap_docu_download( previous = cx_docu ).
    ENDTRY.

  ENDMETHOD.


****************************************************



CLASS lcl_imprimir DEFINITION.
  PUBLIC SECTION.
    CLASS-DATA:    st_control_parameters TYPE   ssfctrlop.
    CLASS-DATA:    st_output_options     TYPE   ssfcompop.
    CLASS-DATA     file_name             TYPE   string.

*----METODOS-------------------------------------------
    "!Envia el SmartForm a un archivo PDF
    CLASS-METHODS imprime_smf_en_pdf RAISING cx_abap_docu_download.
    "!Envia El smartform a impresión Normal
    CLASS-METHODS envia_smf_al_spool
      RETURNING VALUE(r_return) TYPE tsp01-rqident
      RAISING   cx_abap_docu_download.
    "!regresa el Spool_id
    CLASS-METHODS get_spool_id RETURNING VALUE(r_return) TYPE tsp01
                               RAISING   cx_abap_docu_download.
    "!Envia lo que hay en el spool a un archivo pdf
    CLASS-METHODS envia_spool_a_pdf IMPORTING VALUE(i_spool) TYPE tsp01-rqident
                                    RAISING   cx_abap_docu_download.
    CLASS-METHODS set_file_name
      IMPORTING
        i_ivan_file TYPE string.
    CLASS-METHODS set_parametros_impresion.
    CLASS-METHODS envia_spool_a_pdf_server
      IMPORTING i_spool_id TYPE tsp01-rqident
      RAISING   cx_abap_docu_download.

  PRIVATE SECTION.
    CLASS-METHODS get_full_pad
      RETURNING
                VALUE(r_result) TYPE string
      RAISING   cx_abap_docu_download.
    CLASS-METHODS borra_spool.


ENDCLASS.
CLASS lcl_imprimir IMPLEMENTATION.

  METHOD imprime_smf_en_pdf.
* Internal table declaration
    DATA: it_otf   TYPE STANDARD TABLE OF itcoo,
          it_docs  TYPE STANDARD TABLE OF docs,
          it_lines TYPE STANDARD TABLE OF tline.

    DATA:
      st_job_output_info      TYPE ssfcrescl,
      st_document_output_info TYPE ssfcrespd,
      st_job_output_options   TYPE ssfcresop,
      st_output_options       TYPE ssfcompop,
      st_control_parameters   TYPE ssfctrlop,
      v_len_in                TYPE so_obj_len,
      v_language              TYPE sflangu VALUE 'E',
      v_e_devtype             TYPE rspoptype,
      v_bin_filesize          TYPE i,
      v_name                  TYPE string,
      v_path                  TYPE string,
      v_fullpath              TYPE string,
      v_filter                TYPE string,
      v_uact                  TYPE i,
      v_guiobj                TYPE REF TO cl_gui_frontend_services,
      v_filename              TYPE string,
      v_fm_name               TYPE rs38l_fnam.
    CALL FUNCTION 'SSF_GET_DEVICE_TYPE'
      EXPORTING
        i_language    = v_language
        i_application = 'SAPDEFAULT'
      IMPORTING
        e_devtype     = v_e_devtype.
    st_output_options-tdprinter = v_e_devtype.
    st_control_parameters-no_dialog = 'X'.
    st_control_parameters-getotf = 'X'.
*.................GET SMARTFORM FUNCTION MODULE NAME.................*

    CALL FUNCTION 'SSF_FUNCTION_MODULE_NAME'
      EXPORTING
        formname           = 'ZFI011_SF_001'
      IMPORTING
        fm_name            = v_fm_name
      EXCEPTIONS
        no_form            = 1
        no_function_module = 2
        OTHERS             = 3.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.
*...........................CALL SMARTFORM............................*
    CALL FUNCTION v_fm_name
      EXPORTING
        control_parameters   = st_control_parameters
        output_options       = st_output_options
*.....Variables del Smartform
        gs_heder1            = gs_heder1
        gs_heder2            = gs_heder2
        gs_suma              = gs_suma
      IMPORTING
        document_output_info = st_document_output_info
        job_output_info      = st_job_output_info
        job_output_options   = st_job_output_options
      TABLES
*.....Tablas del Smartform
        gt_giro              = gt_giro
      EXCEPTIONS
        formatting_error     = 1
        internal_error       = 2
        send_error           = 3
        user_canceled        = 4
        OTHERS               = 5.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.



*.........................CONVERT TO OTF TO PDF.......................*
    CALL FUNCTION 'CONVERT_OTF_2_PDF'
      IMPORTING
        bin_filesize           = v_bin_filesize
      TABLES
        otf                    = st_job_output_info-otfdata
        doctab_archive         = it_docs
        lines                  = it_lines
      EXCEPTIONS
        err_conv_not_possible  = 1
        err_otf_mc_noendmarker = 2
        OTHERS                 = 3.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.
*........................GET the FILE NAME TO store....................*

    v_name = gv_file_name.
*         .............En el PC ......................
    IF gf_on_serv EQ abap_false.
      v_fullpath = gv_pc_path.
      v_path     = gv_pc_path.
      CREATE OBJECT v_guiobj.
      CALL METHOD v_guiobj->file_save_dialog
        EXPORTING
          default_extension = 'pdf'
          default_file_name = v_name
          file_filter       = v_filter
        CHANGING
          filename          = v_name
          path              = v_path
          fullpath          = v_fullpath
          user_action       = v_uact.
      IF v_uact = v_guiobj->action_cancel.
        EXIT.
      ENDIF.
*..................................DOWNLOAD AS FILE....................*

      MOVE v_fullpath TO v_filename.
      CALL FUNCTION 'GUI_DOWNLOAD'
        EXPORTING
          bin_filesize            = v_bin_filesize
          filename                = v_filename
          filetype                = 'BIN'
        TABLES
          data_tab                = it_lines
        EXCEPTIONS
          file_write_error        = 1
          no_batch                = 2
          gui_refuse_filetransfer = 3
          invalid_type            = 4
          no_authority            = 5
          unknown_error           = 6
          header_not_allowed      = 7
          separator_not_allowed   = 8
          filesize_not_allowed    = 9
          header_too_long         = 10
          dp_error_create         = 11
          dp_error_send           = 12
          dp_error_write          = 13
          unknown_dp_error        = 14
          access_denied           = 15
          dp_out_of_memory        = 16
          disk_full               = 17
          dp_timeout              = 18
          file_not_found          = 19
          dataprovider_exception  = 20
          control_flush_error     = 21
          OTHERS                  = 22.
      IF sy-subrc NE 0.
        RAISE EXCEPTION TYPE cx_abap_docu_download.
      ENDIF.
    ELSE.
*         .............en el servidor ......................

      CONCATENATE gv_serv_path gv_file_name INTO v_fullpath.
      DATA ls_data TYPE tline.


      " Write PDF Binary Data to App Server
      OPEN DATASET v_fullpath FOR OUTPUT IN BINARY MODE.
      IF sy-subrc = 0.
        LOOP AT it_lines INTO ls_data. "INTO wa_binary_data.
          TRANSFER ls_data TO v_fullpath.
        ENDLOOP.
      ENDIF.

      CLOSE DATASET v_fullpath.
      IF sy-subrc EQ 0.
        MESSAGE s034(fes) WITH v_bin_filesize.
      ELSE.
        RAISE EXCEPTION TYPE cx_abap_docu_download.
      ENDIF.
    ENDIF.
  ENDMETHOD.

  METHOD envia_smf_al_spool.
*RETURNING VALUE(r_return) TYPE tsp01-rqident
*RAISING   cx_abap_docu_download.
    CONSTANTS smartform TYPE string  VALUE 'ZFI007_SF_001' .
    DATA: lv_modulo_de_funcion   TYPE rs38l_fnam.
    DATA:
      st_job_output_info      TYPE ssfcrescl,
      st_document_output_info TYPE ssfcrespd,
      st_job_output_options   TYPE ssfcresop.

*.................GET SMARTFORM FUNCTION MODULE NAME.................*
    CALL FUNCTION 'SSF_FUNCTION_MODULE_NAME'
      EXPORTING
        formname           = 'ZFI007_SF_001'
      IMPORTING
        fm_name            = lv_modulo_de_funcion
      EXCEPTIONS
        no_form            = 1
        no_function_module = 2
        OTHERS             = 3.
    IF sy-subrc NE 0.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Error al buscar el SF:'(017).
      sy-msgv2 = smartform.
      sy-msgv3 = 'Exepción no.'(018).
      sy-msgv4 = sy-subrc.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.

    CALL FUNCTION lv_modulo_de_funcion
      EXPORTING
        control_parameters   = lcl_imprimir=>st_control_parameters
        output_options       = lcl_imprimir=>st_output_options
        user_settings        = ' '
*.....Variables del Smartform
        gs_heder1            = gs_heder1
        gs_heder2            = gs_heder2
        gs_suma              = gs_suma
      IMPORTING
        document_output_info = st_document_output_info
        job_output_info      = st_job_output_info
        job_output_options   = st_job_output_options
      TABLES
*.....Tablas del Smartform
        gt_giro              = gt_giro_hoja
      EXCEPTIONS
        formatting_error     = 1
        internal_error       = 2
        send_error           = 3
        user_canceled        = 4
        OTHERS               = 5.
    IF sy-subrc NE 0.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Error al enviar la impresión del SF:'(029).
      sy-msgv2 = smartform.
      sy-msgv3 = 'Error:'(030).
      CASE sy-subrc.
        WHEN 1.
          sy-msgv4 = 'formatting_error'(019).
        WHEN 2.
          sy-msgv4 = 'internal_error'(020).
        WHEN 3.
          sy-msgv4 = 'send_error'(021).
        WHEN 4.
          sy-msgv4 = 'user_canceled'(022).
        WHEN 5.
          sy-msgv4 = 'Causa desconocida 5'(023).
      ENDCASE.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.
    r_return = st_job_output_info-spoolids[ 1 ].
  ENDMETHOD.



  METHOD get_spool_id.
* RETURNING VALUE(r_return) TYPE TSP01_SP0R
* RAISING   cx_abap_docu_download.
*-----------------------------------------
    DATA it_tsp01 TYPE TABLE OF tsp01.
    DATA ls_tsp01 TYPE          tsp01.
    DATA no_spools_user TYPE i.
    DATA fecha_actual   TYPE  tsp01-rqcretime.
    fecha_actual = |{ sy-datum }*|.
    TYPES  tt_tsp01_range      TYPE  RANGE OF tsp01-rqcretime.      "Número de la cuenta de mayor
    DATA(lv_tsp01_range) = VALUE tt_tsp01_range(  ( sign = 'I' option = 'CP' low = fecha_actual ) ).
    WAIT UP TO 1 SECONDS.

    SELECT  FROM tsp01
            FIELDS *
           WHERE rqowner EQ @sy-uname
            AND rqclient EQ @sy-mandt
            AND rqcretime IN @lv_tsp01_range
            ORDER  BY  rqident
            INTO TABLE @DATA(it_spool_user).
    IF sy-subrc EQ 0.
      DESCRIBE TABLE it_spool_user LINES no_spools_user.
      DATA(ls_spool_user) = it_spool_user[ no_spools_user ].
    ENDIF.
    r_return = ls_spool_user.
  ENDMETHOD.

  METHOD envia_spool_a_pdf.
*IMPORTING i_spool TYPE tsp01-rqident
*RAISING   cx_abap_docu_download.
*--------------------------------------
    DATA:i_t001     TYPE TABLE OF t001,
         it_pdf     TYPE TABLE OF tline,
         pdfspoolid TYPE tsp01-rqident,
         g_program  TYPE sy-repid VALUE sy-repid,
         jobname    TYPE tbtcjob-jobname,
         jobcount   TYPE tbtcjob-jobcount,
         numbytes   TYPE i,
         p_file     TYPE string.

    IF i_spool IS INITIAL.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Error No se encontro el Spool para el usuario:'(024).
      sy-msgv2 = sy-uname.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.

    p_file = lcl_imprimir=>get_full_pad( ).
**********************************************************************

    CALL FUNCTION 'CONVERT_OTFSPOOLJOB_2_PDF'
      EXPORTING
        src_spoolid              = i_spool
        no_dialog                = ' '
*       DST_DEVICE               =
*       PDF_DESTINATION          =
      IMPORTING
        pdf_bytecount            = numbytes
        pdf_spoolid              = pdfspoolid
*       OTF_PAGECOUNT            =
        btc_jobname              = jobname
        btc_jobcount             = jobcount
      TABLES
        pdf                      = it_pdf
      EXCEPTIONS
        err_no_otf_spooljob      = 1
        err_no_spooljob          = 2
        err_no_permission        = 3
        err_conv_not_possible    = 4
        err_bad_dstdevice        = 5
        user_cancelled           = 6
        err_spoolerror           = 7
        err_temseerror           = 8
        err_btcjob_open_failed   = 9
        err_btcjob_submit_failed = 10
        err_btcjob_close_failed  = 11.
    IF sy-subrc <> 0.
* Implement suitable error handling here
    ENDIF.
    CALL METHOD cl_gui_frontend_services=>gui_download
      EXPORTING
        filename = p_file
        filetype = 'BIN'
      CHANGING
        data_tab = it_pdf
      EXCEPTIONS
        OTHERS   = 1.

  ENDMETHOD.




  METHOD get_full_pad.
*........................GET the FILE NAME TO store....................*
    DATA:
      v_name     TYPE string,
      v_path     TYPE string,
      v_fullpath TYPE string,
      v_filter   TYPE string,
      v_uact     TYPE i,
      v_guiobj   TYPE REF TO cl_gui_frontend_services,
      v_filename TYPE string,
      v_fm_name  TYPE rs38l_fnam.
    v_name =  | { lcl_imprimir=>file_name } .pdf |.
    CREATE OBJECT v_guiobj.
    CALL METHOD v_guiobj->file_save_dialog
      EXPORTING
        default_extension = 'pdf'
        default_file_name = v_name
        file_filter       = v_filter
      CHANGING
        filename          = v_name
        path              = v_path
        fullpath          = v_fullpath
        user_action       = v_uact.
    IF v_uact = v_guiobj->action_cancel.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Descarga cancelada por el usuario'(025).
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.
    r_result = v_fullpath.
  ENDMETHOD.


  METHOD set_file_name.
*IMPORTING
*    i_ivan_file TYPE string.
*----------------------------
    lcl_imprimir=>file_name =  i_ivan_file.

  ENDMETHOD.


  METHOD set_parametros_impresion.
    DATA   v_e_devtype             TYPE rspoptype.
    DATA   v_language              TYPE sflangu VALUE 'E'.
    "lcl_imprimir=>borra_spool( ).

* Estos parametros sale de ST_JOB_OUTPUT_OPTIONS
* que regresa el smartform de una llamada visible

*    st_output_options-tdcopies = 001.
    lcl_imprimir=>st_output_options-tdarmod = '1'.
    lcl_imprimir=>st_output_options-tddest = 'LOCL'.
    lcl_imprimir=>st_output_options-tdprinter = 'SWIN'.
    lcl_imprimir=>st_output_options-tdlifetime = 8.
    lcl_imprimir=>st_control_parameters-no_dialog = 'X'.
    lcl_imprimir=>st_output_options-tdnewid = 'X'. "Nuevo Spool. MUY IMPORTANTE

  ENDMETHOD.





  METHOD borra_spool.
    DATA:
      ld_rc            TYPE rspotype-rc,
      ld_status        TYPE sy-subrc,
      ld_error_message TYPE rspoemsg,
      lv_spool_id_char TYPE tsp01_sp0r-rqid_char.
    TRY.

        DATA(ls_spool_id) = lcl_imprimir=>get_spool_id( ).
        lv_spool_id_char = ls_spool_id-rqident.

        CALL FUNCTION 'RSPO_R_RDELETE_SPOOLREQ'
          EXPORTING
            spoolid       = lv_spool_id_char
*           allow_commit  =
*           ignore_locks  =
          IMPORTING
            rc            = ld_rc
            status        = ld_status
            error_message = ld_error_message.
      CATCH cx_abap_docu_download INTO DATA(cx_data).
      CLEANUP.
    ENDTRY.
  ENDMETHOD.





  METHOD envia_spool_a_pdf_server.
    CONSTANTS error TYPE string VALUE `Error: ` ##NO_TEXT.
*IMPORTING i_spool_ID TYPE tsp01-rqident
*--------------------------------------
    DATA:i_t001     TYPE TABLE OF t001,
         it_pdf     TYPE TABLE OF tline,
         pdfspoolid TYPE tsp01-rqident,
         g_program  TYPE sy-repid VALUE sy-repid,
         jobname    TYPE tbtcjob-jobname,
         jobcount   TYPE tbtcjob-jobcount,
         numbytes   TYPE i,
         p_file     TYPE string,
         v_fullpath TYPE string.

    IF i_spool_id IS INITIAL.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = TEXT-024. "Error No se encontro el Spool para el usuario:'.
      sy-msgv2 = sy-uname.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.


**********************************************************************

    CALL FUNCTION 'CONVERT_OTFSPOOLJOB_2_PDF'
      EXPORTING
        src_spoolid              = i_spool_id
        no_dialog                = ' '
*       DST_DEVICE               =
*       PDF_DESTINATION          =
      IMPORTING
        pdf_bytecount            = numbytes
        pdf_spoolid              = pdfspoolid
*       OTF_PAGECOUNT            =
        btc_jobname              = jobname
        btc_jobcount             = jobcount
      TABLES
        pdf                      = it_pdf
      EXCEPTIONS
        err_no_otf_spooljob      = 1
        err_no_spooljob          = 2
        err_no_permission        = 3
        err_conv_not_possible    = 4
        err_bad_dstdevice        = 5
        user_cancelled           = 6
        err_spoolerror           = 7
        err_temseerror           = 8
        err_btcjob_open_failed   = 9
        err_btcjob_submit_failed = 10
        err_btcjob_close_failed  = 11.
    IF sy-subrc <> 0.
* Implement suitable error handling here
    ENDIF.

*    CALL METHOD cl_gui_frontend_services=>gui_download
*      EXPORTING
*        filename = p_file
*        filetype = 'BIN'
*      CHANGING
*        data_tab = it_pdf
*      EXCEPTIONS
*        OTHERS   = 1.
    CONCATENATE gv_serv_path gv_file_name INTO v_fullpath.
    DATA ls_data TYPE tline.


    " Write PDF Binary Data to App Server
    OPEN DATASET v_fullpath FOR OUTPUT IN BINARY MODE.
    IF sy-subrc = 0.
      LOOP AT it_pdf INTO ls_data. "INTO wa_binary_data.
        TRANSFER ls_data TO v_fullpath.
      ENDLOOP.
    ELSE.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Error al abrir el dataset'(026).
      sy-msgv2 = v_fullpath.
      sy-msgv3 = |{ error }{ sy-subrc }|.
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.

    CLOSE DATASET v_fullpath.
    IF sy-subrc EQ 0.
      MESSAGE s034(fes) WITH numbytes.
    ELSE.
      sy-msgid = 'sy'.
      sy-msgty = 'I'.
      sy-msgno = 499.
      sy-msgv1 = 'Error al cerrar el dataset'(027).
      sy-msgv2 = 'Para el archivo'(028).
      RAISE EXCEPTION TYPE cx_abap_docu_download.
    ENDIF.


  ENDMETHOD.

ENDCLASS."------ LCL--IMPRIME
