#### Abap_ejemplos_basicos.md

Ejemplos de Abap básicos, para Junniors, como yo, que no quieren serlo toda la vida.

# Archivos en el repositorio

##Ficheros [Carpeta]
Carpeta con Ejemplos sobre el manejo de archivos

### Trabajar con archivos.md
Traducción de un articulo sobre manejo de ficheros. 
[Original Working With Files](https://wiki.scn.sap.com/wiki/display/Snippets/Working+With+Files)

###  zexel2itab.abap
Ejemplo que lee un archivo de Excel y pone los datos en una tabla interna.

Funciones que usa:

- F4_DXFILENAME_4_DYNP
- ALSM_EXCEL_TO_INTERNAL_TABLE

### zitab2text.abap
De tabla interna a archivo de texto

Funciones que usa:

- F4_DXFILENAME_4_DYNP
- GUI_DOWNLOAD

### ztext2itab.abap
De archivo de texto a una tabla interna

Funciones que usa:

- F4_DXFILENAME_4_DYNP
- GUI_UPLOAD

### Uploada2.txt
Archivo de ejemplo para el reporte ztext2itab.abap 