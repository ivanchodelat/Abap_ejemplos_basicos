*&---------------------------------------------------------------------*
*& Report  ZZTEST
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*
REPORT ZZTEST  NO STANDARD PAGE HEADING.

DATA: ITAB LIKE ALSMEX_TABLINE OCCURS 0 WITH HEADER LINE.
TYPES: BEGIN OF T_RECORD,
                  NAME1 LIKE ITAB-VALUE,
                  NAME2 LIKE ITAB-VALUE,
                  AGE LIKE ITAB-VALUE,
              END OF T_RECORD.
DATA: IT_RECORD TYPE STANDARD TABLE OF T_RECORD INITIAL SIZE 0,
            WA_RECORD TYPE T_RECORD.
DATA:       GD_CURRENTROW TYPE I.
PARAMETER P_INFILE LIKE RLGRAP-FILENAME DEFAULT 'C:\TEMP\AA01.xlsx'.
START-OF-SELECTION.
  CALL FUNCTION 'ALSM_EXCEL_TO_INTERNAL_TABLE'
    EXPORTING
      FILENAME                = P_INFILE
      I_BEGIN_COL             = '1'
      I_BEGIN_ROW             = '2' "Do not require headings
      I_END_COL               = '3'
      I_END_ROW               = '13'
    TABLES
      INTERN                  = ITAB
    EXCEPTIONS
      INCONSISTENT_PARAMETERS = 1
      UPLOAD_OLE              = 2
      OTHERS                  = 3.
  IF SY-SUBRC <> 0.
    MESSAGE E010(ZZ) WITH TEXT-001.   "Problem uploading Excel Spreadsheet
  ENDIF.
  SORT ITAB BY ROW COL.
  READ TABLE ITAB INDEX 1.
  GD_CURRENTROW = ITAB-ROW.
  LOOP AT ITAB.
    IF ITAB-ROW NE GD_CURRENTROW.
      APPEND WA_RECORD TO IT_RECORD.
      CLEAR WA_RECORD.
      GD_CURRENTROW = ITAB-ROW.
    ENDIF.
    CASE ITAB-COL.
      WHEN '0001'. "First name
        WA_RECORD-NAME1 = ITAB-VALUE.
      WHEN '0002'. "Surname
        WA_RECORD-NAME2 = ITAB-VALUE.
      WHEN '0003'. "Age
        WA_RECORD-AGE = ITAB-VALUE.
    ENDCASE.
  ENDLOOP.
  APPEND WA_RECORD TO IT_RECORD.
  "Display report data for illustration purposes"
  cl_demo_output=>display( IT_RECORD  ).
*  LOOP AT IT_RECORD INTO WA_RECORD.
*    WRITE:/ SY-VLINE,
*                (10) WA_RECORD-NAME1, SY-VLINE,
*                (10) WA_RECORD-NAME2, SY-VLINE,
*                (10) WA_RECORD-AGE, SY-VLINE.
*  ENDLOOP.