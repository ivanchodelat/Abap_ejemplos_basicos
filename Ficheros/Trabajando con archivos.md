# Trabajar con archivos


>[Original Working With Files](https://wiki.scn.sap.com/wiki/display/Snippets/Working+With+Files)
>Creado por Former Member, modificado por última vez por Gregor Wolf el feb 09, 2016
>Author: Xinpeng Lin



Estes fragmentos de código clarificarán algunos procesos generales de trabajo con ficheros cuando se programan en ABAP. Por ejemplo, carga/descarga de archivos desde/hacia el servidor de aplicaciones/presentaciones, añadiendo la ayuda F4 a la ruta del archivo en la pantalla de selección, comprobando la existencia del archivo.

## Verificar la existencia de un archivo
Antes de procesar el archivo que se introduce en la pantalla de selección por el usuario final, debemos comprobar si existe o no. Si no existe, nos limitamos a dar un mensaje de error y no necesitamos entrar en el programa principal, esta lógica debe hacerse dentro del evento **'AT SELECTION-SCREEN'.**

### archivo del servidor de presentación
Para el archivo del servidor de presentación, aquí intruduce 2 maneras, una es el módulo de funciones, la otra es el método estático de clase.

1. Usando Funciton Moudle 'DX_FILE_EXISTENCE_CHECK'.
 
Error al representar macro 'code': Valor inválido especificado para parámetro 'lang'.

~~~
DATA: gdf_file_exist(1) TYPE c.
PARAMETERS p_file   TYPE dxfile-filename.

AT SELECTION-SCREEN.
CALL FUNCTION 'DX_FILE_EXISTENCE_CHECK'
    EXPORTING
      filename          = p_file
      pc                = 'X'
*   SERVER              =
    IMPORTING
      file_exists       = gdf_file_exist.

IF NOT ( sy-subrc = 0 and gdf_file_exist = 'X' )
  MESSAGE  'the input file does not exist.' TYPE 'E'.
ENDIF.
~~~

Preste atención al parámetro de importación 'pc', debe establecerse como 'X'.
 
2. Uso del método estático de la clase 'CL_GUI_FRONTEND_SERVICES=>FILE_EXIST''.


~~~
DATA: gdf_file_exist(1) TYPE c.
PARAMETERS p_file   TYPE dxfile-filename.

AT SELECTION-SCREEN.
CALL METHOD cl_gui_frontend_services=>file_exist
    EXPORTING
      file             = p_file
    RECEIVING
      result           = gdf_file_exist.

IF NOT ( sy-subrc = 0 and gdf_file_exist = 'X' )
  MESSAGE  'the input file does not exist.' TYPE 'E'.
ENDIF.
~~~

###archivo del servidor de aplicaciones
Para el archivo del servidor de aplicación, generalmente lo abrimos primero dentro del evento 'AT SELECTION-SCREEN'.  Después de abrirlo, no olvide cerrarlo.


~~~
DATA: gdt_ocs_file TYPE TABLE OF ocs_file.
PARAMETERS p_file   TYPE dxfile-filename.

AT SELECTION-SCREEN.
CALL FUNCTION 'OCS_GET_FILE_INFO'
    EXPORTING
      dir_name           = p_file
      file_name          =  '*'
    TABLES
      dir_list               = gdt_ocs_file.
~~~

Importar el parámetro 'nombre_de_archivo' se establece como '*' significa que todo el archivo en el directorio especificado se obtendrá y almacenará en la tabla interna 'gdt_ocs_file'. Si el fichero de entrada se incluye en la tabla interna, este fichero existe.

## Añadir ayuda F4

Añadir una Ayuda F4 a la ruta del archivo en la pantalla de selección será de gran ayuda para el usuario final, la lógica debería estar bajo el evento**'ON VALUE-REQUEST'**.

###archivo del servidor de presentación
Para el archivo del servidor de presentación, aquí intruduce 2 maneras, una es el módulo de funciones, la otra es el método estático de clase.

1. Usando el Módulo Funciton 'F4_FILENAME'.

~~~
PARAMETERS p_file      TYPE dxfile-filename.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.

CALL FUNCTION 'F4_FILENAME'
*   EXPORTING
*     PROGRAM_NAME        =
*     DYNPRO_NUMBER      =
*     FIELD_NAME                =
   IMPORTING
     file_name           = p_file.
~~~

2. Usando el método estático de la clase 'CL_GUI_FRONTEND_SERVICES=>FILE_OPEN_DIALOG'.

~~~
DATA: gdt_filetable TYPE filetable.
DATA: gdf_rc          TYPE I.
PARAMETERS p_file      TYPE dxfile-filename.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.

CALL METHOD cl_gui_frontend_services=>file_open_dialog
   EXPORTING
      WINDOW_TITLE     = 'Choose a file'
   CHANGING
      file_table       = gdt_filetable
      rc               = gdf_rc.
IF sy-subrc = 0.
    READ TABLE gdt_filetable
     INTO gds_filetable  INDEX 1.
    p_file = gds_filetable-filename.
ENDIF.
~~~
gdf_rc es el número del archivo seleccionado, si es igual a -1, se produjo un error.

###archivo del servidor de aplicaciones
Por lo general, no necesita proporcionar la ayuda F4 para el fichero del servidor de aplicación. Si lo queremos, también hay un Módulo Funciotn que puede ser usado.

~~~
PARAMETERS p_file TYPE dxfile-filename.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.

CALL FUNCTION 'F4_DXFILENAME_4_DYNP'
 EXPORTING
      dynpfield_filename = 'P_FILE'
      dyname                    = sy-repid
      dynumb                    = sy-dynnr
      filetype                     = 'P'
      location                    = 'A'
      server                       = '  '.
~~~

**filetype:** 

- 'P' representa el nombre físico del archivo; 
- 'L' - representa el nombre lógico del archivo.

**location:**

- 'A' representa Application Server; 
- 'P' representa Presentation server.

##Descargar Archivos
A veces necesitamos guardar los datos de la tabla interna como un archivo para el proceso posterior. para descargar al servidor de presentación y descargar al servidor de aplicación, son dos tipos diferentes de métodos de proceso.

###al servidor de presentación
Cuando se almacenan datos de tabla internos como un fichero local en el servidor de presentación, hay dos métodos que podemos utilizar.
Uno está usando la función modeule, el otro está usando el método estático de la clase.

**1. Utilización del módulo de funciones 'GUI_DOWNLOAD'**.

~~~
DATA: gdf_filepath string.
DATA: gdt_data type table of gts_data.

START-OF-SELECTION.

gdf_filepath = 'C:\mydata.txt'.

CALL FUNCTION 'GUI_DOWNLOAD'
    EXPORTING
      filename                        = gdf_filepath
      filetype                          = 'ASC'
      write_field_separator   = 'X'
    TABLES
      data_tab                        =  gdt_data.
~~~


el tipo de archivo es establecido en ASC y el separador 'X'  separa las columnas por **Tabs** en caso de descarga ASCII.
Si sy-subrc es igual a 0, la descarga de archivos se realiza correctamente.
 
2. Utilizando el método estático de clase 'CL_GUI_FRONTEND_SERVICES =>GUI_DOWNLOAD'.

~~~
DATA: gdf_filepath type dxfile-filename
DATA: gdt_data type table of gts_data.

START-OF-SELECTION.

gdf_filepath = 'C:\mydata.txt'.

CALL METHOD cl_gui_frontend_services=>gui_download
    EXPORTING
      filename                      = gdf_filepath
      filetype                        = 'ASC'
      write_field_separator  = 'X'
CHANGING
      data_tab                      = gdt_data.
~~~

el tipo de archivo es establecido por 'X' mens separando las columnas por Tabs en caso de descarga ASCII.
Si sy-subrc es igual a 0, la descarga de archivos se realiza correctamente.

##al servidor de aplicaciones <<<<< (A SAP)

Si queremos guardar los datos de la tabla interna en el servidor de aplicación, no hay ningún módulo de funciones o método estático de clase que podamos utilizar, debemos cablear el código por nosotros mismos.

~~~
DATA: gdf_filepath type dxfile-filename
DATA: gdt_data type table of gts_data.
DATA: ldf_length type i.
FIELD-SYMBOLS: <lfs_outfile> TYPE gts_data

START-OF-SELECTION.

  gdf_filepath = 'C:\mydata.txt'.

  OPEN  DATASET gdf_filepath FOR OUTPUT  IN TEXT MODE  ENCODING DEFAULT.

  LOOP AT prt_data ASSIGNING <lfs_outfile>.
    DESCRIBE FIELD <lfs_outfile>  LENGTH ldf_length IN BYTE MODE.
    TRANSFER <lfs_outfile> TO prf_file  LENGTH ldf_length.
  ENDLOOP.

  CLOSE DATASET gdf_filepath.
~~~
El prerrequisito es el campo de prt_data debe ser tipo de carácter.
Usando este método, cada columna de campo saldrá como la longitud definida, sin separador.
 
Si queremos que las columnas de campo estén separadas por tabulaciones, podemos realizarlo de la siguiente manera.

~~~
DATA: gdf_filepath type dxfile-filename
DATA: gdt_data type table of gts_data.
DATA: ldf_length type i.
FIELD-SYMBOLS: <lfs_outfile> TYPE gts_data

START-OF-SELECTION.

gdf_filepath = 'C:\mydata.txt'.

OPEN  DATASET gdf_filepath FOR OUTPUT  IN TEXT MODE  ENCODING DEFAULT.

LOOP AT prt_data ASSIGNING <lfs_outfile>.
        CONCATENATE    <LFS_OUTFILE>-BUKRS
                                        <LFS_OUTFILE>-BUDAT
                                        ...
                           INTO    LDF_OUTDATA
        SEPARATED BY
                           CL_ABAP_CHAR_UTILITIES=>HORIZONTAL_TAB.

        TRANSFER LDF_OUTDATA   TO gdf_filepath.
ENDLOOP.

CLOSE  DATASET gdf_filepath.
~~~

##Subir archivos
A veces tenemos que subir los datos del archivo a la tabla interna primero, y luego procesarlo. para subir desde el servidor de presentación y subir desde el servidor de aplicación, son dos tipos diferentes de métodos de proceso.

###desde el servidor de presentación
Cuando subimos un archivo de datos desde el servidor de presentación a la tabla interna, hay 2 métodos que podemos elegir: uno es usar la función Moduel, el otro es usar el método estático de la clase.
 
**1. Utilizando el módulo de funciones 'GUI_UPLOAD'.**

~~~
DATA: gdt_filedata TYPE TABLE OF gts_filedata.
PARAMETERS p_file TYPE dxfile-filename.

START-OF-SELECTION.
CALL FUNCTION 'GUI_UPLOAD'
      EXPORTING
        filename                = p_file
        has_field_separator     = 'X'
      TABLES
        data_tab                = gdt_filedata.
~~~

el parámetro has_field_separator se establece como 'X' significa columnas separadas por tabulaciones en caso de carga ASCII.

**2. Using the class tatic method 'CL_GUI_FRONTEND_SERVICES =>GUI_UPLOAD'.**

~~~
DATA: gdt_filedata TYPE TABLE OF gts_filedata.
PARAMETERS p_file TYPE dxfile-filename.

START-OF-SELECTION.

CALL METHOD cl_gui_frontend_services=>gui_upload
    EXPORTING
      filename              = p_file
      has_field_separator   = 'X'
    CHANGING
       data_tab             = prt_table.
~~~
 el parámetro "has_field_separator" se define como "X", es decir, columnas separadas por tabulaciones en caso de carga ASCII.

###desde el servidor de aplicaciones
Si queremos cargar datos de fichero desde el servidor de aplicación a la tabla interna, no hay ningún módulo de funciones o método estático de clase que podamos utilizar, debemos cablear el código por nosotros mismos.
 
**1.Para el fichero de datos que no tiene separador entre columnas de campo.
**
~~~
PARAMETERS p_file  TYPE dxfile-filename.

START-OF-SELECTION.

OPEN DATASET p_file IN TEXT MODE ENCODING DEFAULT FOR INPUT.

  DO.
    READ DATASET p_file INTO gds_data.
    IF sy-subrc <> 0.
      EXIT.
    ENDIF.

    APPEND gds_data TO gdt_data.
  ENDDO.

CLOSE DATASET p_file.
~~~

**2.Para el archivo de datos que tiene tabulador separador entre columnas de campo.**

~~~
DATA: gds_field_split type gts_data.
FIELD-SYMBOLS: <fs_field> TYPE gts_data.
PARAMETERS p_file  TYPE dxfile-filename.

START-OF-SELECTION.

OPEN DATASET prf_file IN TEXT MODE ENCODING DEFAULT FOR INPUT.

  DO.
    READ DATASET p_file INTO gds_field.
    SPLIT gds_field  AT cl_abap_char_utilities=>horizontal_tab
         INTO TABLE gdt_field_split.

   LOOP AT gdt_field_split  into gds_field_split.
       gdf_index = gdf_index + 1.
       ASSIGN COMPONENT gdf_index OF STRUCTURE
             gds_data to <fs_field>.

      IF sy-subrc = 0.
          <fs_field> = gds_field_split.
      ENDIF.
   ENDLOOP.

    APPEND gds_data TO gdt_data.
  ENDDO.


CLOSE DATASET p_file.
~~~