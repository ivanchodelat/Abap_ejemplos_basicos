REPORT zficheros.

DATA: it_sflight TYPE STANDARD TABLE OF sflight.
DATA: rex_root TYPE REF TO cx_root. "para apuntar el objeto de la excepcion

PARAMETERS p_file TYPE string DEFAULT 'C:\TEMP\Uploada2.txt'.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.

  CALL FUNCTION 'F4_DXFILENAME_4_DYNP'
    EXPORTING
      dynpfield_filename = 'P_FILE'
      dyname             = sy-repid
      dynumb             = sy-dynnr
      filetype           = 'P'
      location           = 'P'
      server             = '  '.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.



 START-OF-SELECTION.

  TRY.

      CALL FUNCTION 'GUI_UPLOAD'
            EXPORTING
              filename                = p_file
              FILETYPE                      = 'ASC'
              has_field_separator     = 'X'
            TABLES
              data_tab                = it_sflight.
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ELSE.
       cl_demo_output=>display( it_sflight ).
      ENDIF.
      CATCH cx_root INTO rex_root.
        data(text1) = rex_root->get_text( ).
       write:/ 'exception: ', text1.
  ENDTRY.