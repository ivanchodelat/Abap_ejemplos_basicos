*&---------------------------------------------------------------------*
*& Include SIMPLE_TREE_CONTROL_DEMOTOP                                 *
*&                                                                     *
*&---------------------------------------------------------------------*

REPORT SAPSIMPLE_TREE_CONTROL_DEMO MESSAGE-ID TREE_CONTROL_MSG.

  CLASS LCL_APPLICATION DEFINITION DEFERRED.
  CLASS CL_GUI_CFW DEFINITION LOAD.

  TYPES: NODE_TABLE_TYPE LIKE STANDARD TABLE OF MTREESNODE
           WITH DEFAULT KEY.
* CAUTION: MTREESNODE is the name of the node structure which must
* be defined by the programmer. DO NOT USE MTREESNODE!

  DATA: G_APPLICATION TYPE REF TO LCL_APPLICATION,
        G_CUSTOM_CONTAINER TYPE REF TO CL_GUI_CUSTOM_CONTAINER,
        G_TREE TYPE REF TO CL_GUI_SIMPLE_TREE,
        G_OK_CODE TYPE SY-UCOMM.

* Fields on Dynpro 100
  DATA: G_EVENT(30),
        G_NODE_KEY TYPE TV_NODEKEY.

CONSTANTS:
  BEGIN OF c_nodekey,
    root   type tv_nodekey value 'Root',                    "#EC NOTEXT
    child1 TYPE tv_nodekey VALUE 'Child1',                  "#EC NOTEXT
*    child2 type tv_nodekey value 'Child2',                  "#EC NOTEXT
    new1   TYPE tv_nodekey VALUE 'New1',                    "#EC NOTEXT
    new2   TYPE tv_nodekey VALUE 'New2',                    "#EC NOTEXT
*    new3   type tv_nodekey value 'New3',                    "#EC NOTEXT
*    new4   type tv_nodekey value 'New4',                    "#EC NOTEXT
  END OF c_nodekey.

*** INCLUDE SIMPLE_TREE_CONTROL_DEMOTOP